//
//  CountriesViewController.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import Alamofire
import CoreData

class CountriesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var countries = [Country]()
    var previousCellIndexPath: NSIndexPath?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        downloadData {
            self.tableView.reloadData()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        tableView.reloadData()
    }
    
    // MARK: TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return countries.count
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return (countries[section].collapsed!) ? 0 : countries[section].cities.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("CityCell") as? CityCell {
            let city = countries[indexPath.section].cities[indexPath.row]
            cell.configureCell(city)
            if defaults.valueForKey("SelectedCity") != nil {
                if city.id == defaults.valueForKey("SelectedCity") as! Int {
                    cell.makeActive()
                    previousCellIndexPath = indexPath
                }
            }
            return cell
        } else {
            return CityCell()
        }
    }
    
    func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        if let header = tableView.dequeueReusableCellWithIdentifier("CountryCell") as? CountryCell {
            header.toggleButton.tag = section
            header.nameLabel.text = countries[section].name
            header.toggleButton.addTarget(self, action: #selector(CountriesViewController.toggleCollapse), forControlEvents: .TouchUpInside)
            return header.contentView
        } else {
            return UIView()
        }
        
    }
    
    func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40.0
    }
    
    func toggleCollapse(sender: UIButton) {
        let section = sender.tag
        let collapsed = countries[section].collapsed
        countries[section].collapsed = !collapsed
        tableView.reloadSections(NSIndexSet(index: section), withRowAnimation: .Automatic)
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let cell = tableView.cellForRowAtIndexPath(indexPath) as? CityCell {
            
            if previousCellIndexPath != nil {
                if let previousCell = tableView.cellForRowAtIndexPath(previousCellIndexPath!) as? CityCell {
                    previousCell.makeInactive()
                }
            }
            
            cell.makeActive()
            previousCellIndexPath = indexPath
            let city = countries[indexPath.section].cities[indexPath.row]
            defaults.setValue(city.id, forKey: "SelectedCity")
        }
    }
    
    // MARK: Data
    
    struct Country {
        var name: String!
        var cities: [City]!
        var collapsed: Bool!
        
        init(name: String, cities: [City], collapsed: Bool = true) {
            self.name = name
            self.cities = cities
            self.collapsed = collapsed
        }
    }
    
    func downloadData(completed: ()->()) {
        
        let url = "https://atw-backend.azurewebsites.net/api/countries"
        Alamofire.request(.GET, url)
            .responseJSON { response in
                if let JSON = response.result.value as? Dictionary<String, AnyObject> {
                    if let result = JSON["Result"] as? [Dictionary<String, AnyObject>] {
                        for country in result {
                            if let countryName = country["Name"] as? String {
                                var citiesToAppend = [City]()
                                if let cities = country["Cities"] as? [Dictionary<String, AnyObject>] {
                                    for city in cities {
                                        if let cityName = city["Name"] as? String {
                                            if let cityID = city["Id"] as? Int {
                                                let city = City(id: cityID, name: cityName)
                                                citiesToAppend.append(city)
                                            }
                                        }
                                    }
                                    let countryToAppend = Country(name: countryName, cities: citiesToAppend)
                                    self.countries.append(countryToAppend)
                                }
                            }
                        }
                        completed()
                    }
                }
        }
    }
    
}
