//
//  City.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import Foundation

class City {
    
    private var _id: Int!
    private var _name: String!
    
    var id: Int {
        return _id
    }
    
    var name: String {
        return _name
    }
    
    init(id: Int, name: String) {
        self._id = id
        self._name = name
    }
    
}