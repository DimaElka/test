//
//  FavoritesViewController.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import CoreData

class FavoritesViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    var favoritedCities = [City]()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tableView.delegate = self
        tableView.dataSource = self
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reloadData:", name:"ReloadTable", object: nil)
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        favoritedCities = []
        fetchData {
            self.tableView.reloadData()
        }
        
    }
    
    // MARK: TableView
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favoritedCities.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if let cell = tableView.dequeueReusableCellWithIdentifier("simpleCityCell") as? simpleCityCell {
            let city = favoritedCities[indexPath.row]
            cell.configureCell(city)
            return cell
        } else {
            return simpleCityCell()
        }
    }
    
    // MARK: Data
    
    func fetchData(completed: ()->()) {
        
        var cities = [NSManagedObject]()
        
        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let managedContext = appDelegate.managedObjectContext
        let fetchRequest = NSFetchRequest(entityName: "City")
        
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            cities = results as! [NSManagedObject]
        } catch let error as NSError {
            print("Error in CoreData fetching (\(error), \(error.userInfo))")
        }
        
        for city in cities {
            if let id = city.valueForKey("id") as? Int {
                if let name = city.valueForKey("name") as? String {
                    let objectToAppend = City(id: id, name: name)
                    self.favoritedCities.append(objectToAppend)
                }
            }
        }
        
        completed()
    }
    
    func reloadData(notification: NSNotification) {
        favoritedCities = []
        fetchData { 
            self.tableView.reloadData()
        }
    }
    
}
