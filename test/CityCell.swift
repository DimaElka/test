//
//  CityCell.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import CoreData

class CityCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var favoriteBtn: UIButton!
    
    var id: Int?
    var name: String?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    @IBAction func favoritePressed(sender: AnyObject) {
        
        if id != nil && name != nil {
            let cityID = id!
            let cityName = name!
            if isFavorite(cityID) {
                let predicate = NSPredicate(format: "id == %@", "\(cityID)")
                let fetchRequest = NSFetchRequest(entityName: "City")
                fetchRequest.predicate = predicate
                
                do {
                    let results = try managedContext.executeFetchRequest(fetchRequest)
                    let city = results as! [NSManagedObject]
                    if city.count > 0 {
                        managedContext.deleteObject(city.first!)
                        unfavorite()
                    } else {
                        print("Error while removing")
                        unfavorite()
                    }
                    
                } catch let error as NSError {
                    print("\(error), \(error.userInfo)")
                }
                
                do {
                    try managedContext.save()
                } catch {
                    print("Core Data error while saving")
                }
                
            } else {
                
                let entity = NSEntityDescription.entityForName("City", inManagedObjectContext: managedContext)
                let city = NSManagedObject(entity: entity!, insertIntoManagedObjectContext: managedContext)
                city.setValue(cityName, forKey: "name")
                city.setValue(id, forKey: "id")
                
                do {
                    try managedContext.save()
                    
                    favorite()
                    
                } catch let error as NSError {
                    print("Core Data save error: (\(error), \(error.userInfo))")
                }
            }
            
        }
        
    }
    
    func isFavorite(id: Int) -> Bool {
        let predicate = NSPredicate(format: "id == %@", "\(id)")
        let fetchRequest = NSFetchRequest(entityName: "City")
        fetchRequest.predicate = predicate
        do {
            let results = try managedContext.executeFetchRequest(fetchRequest)
            let city = results as! [NSManagedObject]
            if city.count > 0 {
                return true
            } else {
                return false
            }
            
        } catch let error as NSError {
            print("\(error), \(error.userInfo)")
            return false
        }
    }
    
    func configureCell(city: City) {
        
        makeInactive()
        
        self.nameLabel.text = city.name
        self.name = city.name
        self.id = city.id
        
        if isFavorite(city.id) {
            self.favorite()
        } else {
            self.unfavorite()
        }
        
    }
    
    func makeActive() {
        self.backgroundColor = UIColor.lightGrayColor()
    }
    
    func makeInactive() {
        self.backgroundColor = UIColor.whiteColor()
    }
    
    func favorite() {
        self.favoriteBtn.setImage(UIImage(named: "icon-filled"), forState: .Normal)
    }
    
    func unfavorite() {
        self.favoriteBtn.setImage(UIImage(named: "icon"), forState: .Normal)
    }
    
}
