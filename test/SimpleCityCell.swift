//
//  simpleCityCell.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import CoreData

class simpleCityCell: UITableViewCell {
    
    @IBOutlet weak var cityNameLabel: UILabel!
    
    var id: Int?

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func configureCell(city: City) {
        self.cityNameLabel.text = city.name
        self.id = city.id
    }

    @IBAction func removeFromFavorites(sender: AnyObject) {
        
        if let cityID = id {
            let predicate = NSPredicate(format: "id == %@", "\(cityID)")
            let fetchRequest = NSFetchRequest(entityName: "City")
            fetchRequest.predicate = predicate
            
            do {
                let results = try managedContext.executeFetchRequest(fetchRequest)
                let city = results as! [NSManagedObject]
                if city.count > 0 {
                    managedContext.deleteObject(city.first!)
                } else {
                    print("Error while removing")
                }
                
            } catch let error as NSError {
                print("\(error), \(error.userInfo)")
            }
            
            do {
                try managedContext.save()
            } catch {
                print("Core Data error while saving")
            }
            
            NSNotificationCenter.defaultCenter().postNotificationName("ReloadTable", object: nil)
            
        }
    }
}
