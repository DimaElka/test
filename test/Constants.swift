//
//  Constants.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit
import Foundation
import CoreData

let defaults = NSUserDefaults.standardUserDefaults()
let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
let managedContext = appDelegate.managedObjectContext