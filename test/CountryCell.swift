//
//  CountryCell.swift
//  test
//
//  Created by Rogov Dmitiy on 20.09.16.
//  Copyright © 2016 Rogov Dmitry. All rights reserved.
//

import UIKit

class CountryCell: UITableViewCell {
    
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var toggleButton: UIButton!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
}
